package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class DoneListActivity extends AppCompatActivity {

    private DoneAdapter dataAdapter;
    private ArrayList<DoneTask> doneList = new ArrayList<>();
    private int showCheck;
    private Menu menu;
    private AlertDialog alertDialog;
    private int alertDialogType;
    public static final int DELETE_ALERT_DIALOG_SHOW = 1;
    public static final int DONT_SHOW_ALERT_DIALOG = 0;
    public static final int DISPLAY_CHECKBOX = 1;
    public static final int DONT_DISPLAY_CHECKBOX = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done_list);

        displayListView();

        if (savedInstanceState != null) {
            alertDialogType = savedInstanceState.getInt("alertDialogType");
        }

        Toolbar doneToolbar = (Toolbar) findViewById(R.id.done_toolbar);

        setSupportActionBar(doneToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        SharedPreferences sp = getSharedPreferences("checkBox_mode_done_list", Activity.MODE_PRIVATE);
        showCheck = sp.getInt("showCheck", 0);

        if(alertDialogType == DELETE_ALERT_DIALOG_SHOW) deleteDialog();

    }

    private void displayListView() {

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        doneList = dbHelper.getInfoFromDoneList();

        dataAdapter = new DoneAdapter(this, R.layout.list_item3, doneList);

        ListView doneListView = (ListView) findViewById(R.id.done_list_view);
        doneListView.setAdapter(dataAdapter);

        doneListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {

                if(showCheck == DONT_DISPLAY_CHECKBOX) {

                    Intent myIntent = new Intent(DoneListActivity.this, DoneDetailsPanelActivity.class);
                    myIntent.putExtra("position", position);
                    startActivity(myIntent);
                    finish();

                }
            }
        });

        dbHelper.closeConnection(dbHelper);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                showCheck = DISPLAY_CHECKBOX;
                showCheckBox();
                Intent myIntent = new Intent(DoneListActivity.this, ReminderPanelActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.check:
                showCheckBox();
                return true;

            case R.id.delete:
                deleteDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.menu = menu;
        getMenuInflater().inflate(R.menu.done_toolbar_items, menu);

        if(showCheck == DISPLAY_CHECKBOX) setOptionTitle(R.id.check, "Wstecz");

        return true;
    }

    private void hideOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

    private void setOptionTitle(int id, String title)
    {
        MenuItem item = menu.findItem(id);
        item.setTitle(title);
    }

    private void setOptionIcon(int id, int iconRes)
    {
        MenuItem item = menu.findItem(id);
        item.setIcon(iconRes);
    }

    public void showCheckBox() {

        if(showCheck == DONT_DISPLAY_CHECKBOX) {

            showCheck = DISPLAY_CHECKBOX;

            SharedPreferences sp = getSharedPreferences("checkBox_mode_done_list", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putInt("showCheck", showCheck);
            editor.commit();

            displayListView();

            setOptionTitle(R.id.check, "Wstecz");


        } else if(showCheck == DISPLAY_CHECKBOX) {

            showCheck = DONT_DISPLAY_CHECKBOX;

            SharedPreferences sp = getSharedPreferences("checkBox_mode_done_list", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putInt("showCheck", showCheck);
            editor.commit();

            DataBaseHelper dbHelper = new DataBaseHelper(this);
            dbHelper.deleteDoneChecks();
            dbHelper.closeConnection(dbHelper);

            displayListView();

            setOptionTitle(R.id.check, "Zaznacz");

        }
    }

    public void deleteDialog() {

        if(showCheck == DONT_DISPLAY_CHECKBOX) {

            Toast.makeText(getApplicationContext(),
                    "Wybierz zadnia do usunięcia", Toast.LENGTH_LONG).show();

        } else if(showCheck == DISPLAY_CHECKBOX) {

            alertDialogType = DELETE_ALERT_DIALOG_SHOW;

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.delete_ask_panel, null);
            builder.setView(dialogView);

            alertDialog = builder.create();
            alertDialog.show();
        }

    }

    public void deleteData() {


        final ArrayList<DoneTask> doneList = this.doneList;
        final DataBaseHelper dbHelper = new DataBaseHelper(this);

        for (int i = 0; i < doneList.size(); i++) {

            final DoneTask doneTask = doneList.get(i);

            if (doneTask.isSelected()) {

                dbHelper.deleteDoneRecord(doneTask);
            }
        }

        dbHelper.deleteDoneChecks();

        showCheck = DISPLAY_CHECKBOX;
        showCheckBox();

        displayListView();

        dbHelper.closeConnection(dbHelper);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(alertDialogType == DELETE_ALERT_DIALOG_SHOW) alertDialog.cancel();

        savedInstanceState.putInt("alertDialogType", alertDialogType);
    }

    @Override
    public void onBackPressed() {
        showCheck = DISPLAY_CHECKBOX;
        showCheckBox();
        System.exit(0);
    }

    public void delete_yes(View view) {
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        deleteData();
        alertDialog.cancel();
    }

    public void delete_no(View view) {
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();
    }
}
