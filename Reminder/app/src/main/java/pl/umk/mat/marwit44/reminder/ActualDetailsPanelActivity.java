package pl.umk.mat.marwit44.reminder;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ActualDetailsPanelActivity extends AppCompatActivity {

    private int position;
    private Menu menu;
    private ActualTask actualTask;
    private AlertDialog alertDialog;
    private int alertDialogType;
    public static final int ACCEPT_ONE_ALERT_DIALOG_SHOW = 1;
    public static final int DONT_SHOW_ALERT_DIALOG = 0;
    public static final int TASK_TYPE_REMIND = 0;
    public static final int TASK_TYPE_EVENT = 1;
    public static final int TASK_TYPE_MEET = 2;
    public static final int TASK_TYPE_BIRTHDAY = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actual_details_panel);

        Toolbar actualDetailsToolbar = (Toolbar) findViewById(R.id.actual_details_toolbar);

        setSupportActionBar(actualDetailsToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (savedInstanceState != null) {
            alertDialogType = savedInstanceState.getInt("alertDialogType");
        }

        if(alertDialogType == ACCEPT_ONE_ALERT_DIALOG_SHOW) acceptOneDialog();

        Intent myIntent = getIntent();

        position = myIntent.getIntExtra("position",0);

        setData(position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.show_det_ac_toolbar_items, menu);
        return true;
    }

    public void setData(int position) {

        DataBaseHelper dbHelper = new DataBaseHelper(this);
        ArrayList<ActualTask> actualList = new ArrayList<>();

        actualList = dbHelper.getInfoFromActualList();

        actualTask = actualList.get(position);

        TextView titleTextActual = (TextView) findViewById(R.id.title_text_ac);
        TextView detailsTextActual = (TextView) findViewById(R.id.details_text_ac);
        TextView dataTextActual = (TextView) findViewById(R.id.data_text_ac);

        titleTextActual.setText(actualTask.getTitle());
        detailsTextActual.setText(actualTask.getDetails());
        dataTextActual.setText(actualTask.getAlarmTimeText());

        FrameLayout taskTypeFrameActual = (FrameLayout) findViewById(R.id.task_type_frame_ac);

        int type = actualTask.getType();
        setTaskTypeInFrame(taskTypeFrameActual,type);

        dbHelper.closeConnection(dbHelper);

    }

    public void setTaskTypeInFrame(FrameLayout taskTypeFrame, int type) {

        if(type == TASK_TYPE_REMIND) taskTypeFrame.setBackgroundResource(R.drawable.przypomnienie2);
        else if(type == TASK_TYPE_EVENT) taskTypeFrame.setBackgroundResource(R.drawable.wydarzenie2);
        else if(type == TASK_TYPE_MEET) taskTypeFrame.setBackgroundResource(R.drawable.spotkanie3);
        else if(type == TASK_TYPE_BIRTHDAY) taskTypeFrame.setBackgroundResource(R.drawable.urodziny2);

    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                Intent myIntent = new Intent(ActualDetailsPanelActivity.this, ActualListActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.accept_one:
                acceptOneDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void acceptOneDialog() {

        alertDialogType = ACCEPT_ONE_ALERT_DIALOG_SHOW;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.accept_one_ask_panel, null);
        builder.setView(dialogView);

        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(alertDialogType == ACCEPT_ONE_ALERT_DIALOG_SHOW) alertDialog.cancel();

        savedInstanceState.putInt("alertDialogType", alertDialogType);
    }

    public void acceptOne() {

        final DataBaseHelper dbHelper = new DataBaseHelper(this);

        dbHelper.deleteActualRecord(actualTask);
        dbHelper.addDoneRecord(actualTask);

        dbHelper.closeConnection(dbHelper);
    }

    public void accept_one_yes(View view) {
        acceptOne();
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();

        Intent myIntent = new Intent(ActualDetailsPanelActivity.this, ActualListActivity.class);
        startActivity(myIntent);
        finish();
    }

    public void accept_one_no(View view) {
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();
    }
}
