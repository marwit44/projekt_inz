package pl.umk.mat.marwit44.reminder;

import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class CounterStartPanelActivity extends AppCompatActivity {

    private int step = 1;
    private int state = 0;
    private int position = 0;
    private int changeState = 0;
    private AlertDialog alertDialog;
    private EditText changeStepEditText;
    private Counter counter;
    private int changeOrient;
    private int alertDialogType;
    private String stateText;
    private String stepText;
    public static final int CHANGE_STEP_ALERT_DIALOG_SHOW = 1;
    public static final int CHANGE_STATE_ALERT_DIALOG_SHOW = 2;
    public static final int DONT_SHOW_ALERT_DIALOG = 0;
    public static final int ORIENTATION_NOT_CHANGED = 0;
    public static final int ORIENTATION_CHANGED = 1;
    public static final int DONOT_CHANGE_STATE = 0;
    public static final int CHANGING_STATE_NOW = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter_start_panel);

        Toolbar counterStartPanelToolbar = (Toolbar) findViewById(R.id.counter_start_panel_toolbar);

        setSupportActionBar(counterStartPanelToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent myIntent2 = getIntent();
        position = myIntent2.getIntExtra("position",0);

        if (savedInstanceState != null) {

            alertDialogType = savedInstanceState.getInt("alertDialogType");

            state = savedInstanceState.getInt("state");
            step = savedInstanceState.getInt("step");
            changeOrient = savedInstanceState.getInt("changeOrient");

            if(alertDialogType == CHANGE_STATE_ALERT_DIALOG_SHOW) stateText = savedInstanceState.getString("stateText");
            if(alertDialogType == CHANGE_STEP_ALERT_DIALOG_SHOW) stepText = savedInstanceState.getString("stepText");
        }

        if(alertDialogType == CHANGE_STEP_ALERT_DIALOG_SHOW) {

            changeStepDialog();

            } else if(alertDialogType == CHANGE_STATE_ALERT_DIALOG_SHOW) {

                changeStateDialog();
            }

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        ArrayList<Counter> counterList = dbHelper.getInfoFromCounterList();

        counter = counterList.get(position);


        if(changeOrient == ORIENTATION_NOT_CHANGED) {

            state = counter.getState();
            step = counter.getStep();

        }

        TextView counterNameText = (TextView) findViewById(R.id.counter_name_text);
        counterNameText.setText("" + counter.getTitle());

        TextView counterStepText = (TextView) findViewById(R.id.counter_step_text);
        counterStepText.setText("" + step);

        Button stateButton = (Button) findViewById(R.id.state_button);
        stateButton.setText("" + state);

        dbHelper.closeConnection(dbHelper);

        Button minusButton = (Button) findViewById(R.id.minus_button);
        minusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView counterStepText = (TextView) findViewById(R.id.counter_step_text);
                step --;
                counterStepText.setText("" + step);
            }
        });

        Button plusButton = (Button) findViewById(R.id.plus_button);
        plusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TextView counterStepText = (TextView) findViewById(R.id.counter_step_text);
                step ++;
                counterStepText.setText("" + step);
            }
        });


        stateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Button stateButton = (Button) findViewById(R.id.state_button);
                String state2 = stateButton.getText().toString();
                state = Integer.parseInt(state2);

                state += step;
                stateButton.setText("" + state);
            }
        });

        stateButton.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                changeStateDialog();

                return true;
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.counter_start_toolbar_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                DataBaseHelper dbHelper = new DataBaseHelper(this);
                counter.setState(state);
                counter.setStep(step);

                dbHelper.updateCounterRecord(counter);
                dbHelper.closeConnection(dbHelper);

                Intent myIntent = new Intent(CounterStartPanelActivity.this, StartPanelActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.showList:

                DataBaseHelper dbHelper2 = new DataBaseHelper(this);
                counter.setState(state);
                counter.setStep(step);

                dbHelper2.updateCounterRecord(counter);
                dbHelper2.closeConnection(dbHelper2);

                Intent myIntent2 = new Intent(CounterStartPanelActivity.this, CounterListActivity.class);
                startActivity(myIntent2);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void changeStepDialogView(View view) {

        changeStepDialog();
    }

    public void changeStepDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.change_step_panel, null);
        builder.setView(dialogView);

        changeStepEditText =(EditText) dialogView.findViewById(R.id.change_step_editText);
        changeStepEditText.setText("" + step);

        if(alertDialogType == CHANGE_STEP_ALERT_DIALOG_SHOW) changeStepEditText.setText("" + Integer.parseInt(stepText));

        alertDialogType = CHANGE_STEP_ALERT_DIALOG_SHOW;

        alertDialog = builder.create();
        alertDialog.show();
    }

    public void change_yes(View view) {

        getStep();

        stepText = "" + step;
        stateText = "" + state;

        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();
    }

    public void change_no(View view) {

        stepText = "" + step;
        stateText = "" + state;

        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();

    }

    public void getStep() {

        String temp = changeStepEditText.getText().toString();

        if(changeState == DONOT_CHANGE_STATE) {

            step = Integer.parseInt(temp);

            TextView counterStepText = (TextView) findViewById(R.id.counter_step_text);
            counterStepText.setText("" + step);

        } else if(changeState == CHANGING_STATE_NOW) {

                    state = Integer.parseInt(temp);

                    Button stateButton = (Button) findViewById(R.id.state_button);
                    stateButton.setText("" + state);

                    changeState = DONOT_CHANGE_STATE;

                }

    }

    public void changeStateDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.change_step_panel, null);
        builder.setView(dialogView);

        changeStepEditText =(EditText) dialogView.findViewById(R.id.change_step_editText);
        changeStepEditText.setText("" + state);

        changeState = CHANGING_STATE_NOW;

        if(alertDialogType == CHANGE_STATE_ALERT_DIALOG_SHOW) changeStepEditText.setText("" + Integer.parseInt(stateText));

        alertDialogType = CHANGE_STATE_ALERT_DIALOG_SHOW;

        alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(alertDialogType == CHANGE_STEP_ALERT_DIALOG_SHOW) alertDialog.cancel();
            else if(alertDialogType == CHANGE_STATE_ALERT_DIALOG_SHOW) alertDialog.cancel();

        savedInstanceState.putInt("alertDialogType", alertDialogType);
        savedInstanceState.putInt("state", state);
        savedInstanceState.putInt("step", step);

        if(alertDialogType == CHANGE_STATE_ALERT_DIALOG_SHOW) savedInstanceState.putString("stateText", changeStepEditText.getText().toString());
        if(alertDialogType == CHANGE_STEP_ALERT_DIALOG_SHOW) savedInstanceState.putString("stepText", changeStepEditText.getText().toString());

        savedInstanceState.putInt("changeOrient", ORIENTATION_CHANGED);
    }

    @Override
    public void onBackPressed() {

        DataBaseHelper dbHelper = new DataBaseHelper(this);
        counter.setState(state);
        counter.setStep(step);

        dbHelper.updateCounterRecord(counter);
        dbHelper.closeConnection(dbHelper);

        System.exit(0);
    }
}
