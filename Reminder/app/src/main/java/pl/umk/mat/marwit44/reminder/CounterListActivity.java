package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class CounterListActivity extends AppCompatActivity {

    private CounterAdapter dataAdapter;
    private ArrayList<Counter> counterList = new ArrayList<>();
    private int showCheck;
    private Menu menu;
    private AlertDialog alertDialog;
    private AlertDialog alertDialog2;
    private EditText addCounterEditText;
    private int alertDialogType;
    private String title;
    private int edit;
    private int pos;
    private Counter counter;
    public static final int DELETE_ALERT_DIALOG_SHOW = 1;
    public static final int ADD_ALERT_DIALOG_SHOW = 2;
    public static final int DONT_SHOW_ALERT_DIALOG = 0;
    public static final int EDIT_COUNTER_NAME = 1;
    public static final int DONT_EDIT_COUNTER_NAME = 0;
    public static final int DISPLAY_CHECKBOX = 1;
    public static final int DONT_DISPLAY_CHECKBOX = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter_list);

        displayListView();

        Toolbar counterToolbar = (Toolbar) findViewById(R.id.counter_toolbar);

        setSupportActionBar(counterToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        SharedPreferences sp = getSharedPreferences("checkBox_mode_counter_list", Activity.MODE_PRIVATE);
        showCheck = sp.getInt("showCheck", 0);

        if (savedInstanceState != null) {

            alertDialogType = savedInstanceState.getInt("alertDialogType");
            edit = savedInstanceState.getInt("edit");

            if(alertDialogType == ADD_ALERT_DIALOG_SHOW) title = savedInstanceState.getString("title");
            if(edit == EDIT_COUNTER_NAME) pos = savedInstanceState.getInt("pos");

        }

        if(alertDialogType == DELETE_ALERT_DIALOG_SHOW) deleteDialog();
            else if(alertDialogType == ADD_ALERT_DIALOG_SHOW) {
                addDialog();
            }

    }

    private void displayListView() {

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        counterList = dbHelper.getInfoFromCounterList();

        dataAdapter = new CounterAdapter(this, R.layout.list_item, counterList);

        ListView counterListView = (ListView) findViewById(R.id.counter_list_view);
        counterListView.setAdapter(dataAdapter);

        counterListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {

                if(showCheck == DONT_DISPLAY_CHECKBOX) {

                    Intent myIntent = new Intent(CounterListActivity.this, CounterStartPanelActivity.class);
                    myIntent.putExtra("position", position);
                    startActivity(myIntent);
                    finish();
                }
            }
        });

        counterListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> listView, View view,
                                           int position, long id) {

                if(showCheck == DONT_DISPLAY_CHECKBOX) {

                    edit = EDIT_COUNTER_NAME;
                    pos = position;
                    addDialog();
                }

                return true;
            }
        });

        dbHelper.closeConnection(dbHelper);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                showCheck = DISPLAY_CHECKBOX;
                showCheckBox();

                Intent myIntent = new Intent(CounterListActivity.this, StartPanelActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.add:
                showCheck = DISPLAY_CHECKBOX;
                showCheckBox();
                addDialog();
                return true;

            case R.id.check:
                showCheckBox();
                return true;

            case R.id.delete:
                deleteDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.menu = menu;
        getMenuInflater().inflate(R.menu.counter_toolbar_items, menu);

        if(showCheck == DISPLAY_CHECKBOX) setOptionTitle(R.id.check, "Wstecz");

        return true;
    }

    private void hideOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

    private void setOptionTitle(int id, String title)
    {
        MenuItem item = menu.findItem(id);
        item.setTitle(title);
    }

    private void setOptionIcon(int id, int iconRes)
    {
        MenuItem item = menu.findItem(id);
        item.setIcon(iconRes);
    }

    public void showCheckBox() {

        if(showCheck == DONT_DISPLAY_CHECKBOX) {

            showCheck = DISPLAY_CHECKBOX;

            SharedPreferences sp = getSharedPreferences("checkBox_mode_counter_list", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putInt("showCheck", showCheck);
            editor.commit();

            displayListView();

            setOptionTitle(R.id.check, "Wstecz");


        } else if(showCheck == DISPLAY_CHECKBOX) {

            showCheck = DONT_DISPLAY_CHECKBOX;

            SharedPreferences sp = getSharedPreferences("checkBox_mode_counter_list", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putInt("showCheck", showCheck);
            editor.commit();

            DataBaseHelper dbHelper = new DataBaseHelper(this);

            dbHelper.deleteCounterChecks();
            dbHelper.closeConnection(dbHelper);

            displayListView();

            setOptionTitle(R.id.check, "Zaznacz");

        }
    }

    public void deleteDialog() {

        if(showCheck == DONT_DISPLAY_CHECKBOX) {

            Toast.makeText(getApplicationContext(),
                    "Wybierz zadnia do usunięcia", Toast.LENGTH_LONG).show();

        } else if(showCheck == DISPLAY_CHECKBOX) {

            alertDialogType = DELETE_ALERT_DIALOG_SHOW;

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.delete_ask_panel, null);
            builder.setView(dialogView);

            alertDialog = builder.create();
            alertDialog.show();
        }

    }

    public void deleteData() {


        final ArrayList<Counter> counterList = this.counterList;
        final DataBaseHelper dbHelper = new DataBaseHelper(this);

        for (int i = 0; i < counterList.size(); i++) {

            final Counter counter = counterList.get(i);

            if (counter.isSelected()) {
                dbHelper.deleteCounterRecord(counter);
            }

        }

        dbHelper.deleteCounterChecks();

        showCheck = DISPLAY_CHECKBOX;
        showCheckBox();

        displayListView();

        dbHelper.closeConnection(dbHelper);
    }

    public void addDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.add_counter_panel, null);
        builder.setView(dialogView);

        addCounterEditText =(EditText) dialogView.findViewById(R.id.add_counter_editText);

        if(edit == EDIT_COUNTER_NAME) {
            DataBaseHelper dbHelper = new DataBaseHelper(this);

            ArrayList<Counter> cnounterList = dbHelper.getInfoFromCounterList();

            counter = cnounterList.get(pos);
            addCounterEditText.setText(counter.getTitle());

            dbHelper.closeConnection(dbHelper);
        }

        if(alertDialogType == ADD_ALERT_DIALOG_SHOW) addCounterEditText.setText(title);

        alertDialogType = ADD_ALERT_DIALOG_SHOW;

        alertDialog2 = builder.create();
        alertDialog2.show();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(alertDialogType == DELETE_ALERT_DIALOG_SHOW) alertDialog.cancel();
            else if(alertDialogType == ADD_ALERT_DIALOG_SHOW) alertDialog2.cancel();

        savedInstanceState.putInt("alertDialogType", alertDialogType);
        savedInstanceState.putInt("edit", edit);

        if(alertDialogType == ADD_ALERT_DIALOG_SHOW) savedInstanceState.putString("title", addCounterEditText.getText().toString());
        if(edit == EDIT_COUNTER_NAME) savedInstanceState.putInt("pos", pos);
    }

    @Override
    public void onBackPressed() {
        showCheck = DISPLAY_CHECKBOX;
        showCheckBox();

        System.exit(0);
    }

    public void delete_yes(View view) {
        deleteData();
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();
    }

    public void delete_no(View view) {
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();
    }

    public void add_yes(View view) {

        title = addCounterEditText.getText().toString();
        DataBaseHelper dbHelper = new DataBaseHelper(this);

        if(edit == DONT_EDIT_COUNTER_NAME) {
            dbHelper.addCounterRecord(title);

        } else if(edit == EDIT_COUNTER_NAME) {
                    dbHelper.updateCounterTitle(counter,title);
                }

        dbHelper.closeConnection(dbHelper);

        displayListView();

        edit = DONT_EDIT_COUNTER_NAME;
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        title = "";
        alertDialog2.cancel();
    }

    public void add_no(View view) {

        edit = DONT_EDIT_COUNTER_NAME;
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        title = "";
        alertDialog2.cancel();
    }
}
