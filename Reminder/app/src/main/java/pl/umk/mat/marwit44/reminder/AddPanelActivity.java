package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class AddPanelActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    WaitingTask waitingTask = new WaitingTask();
    WaitingTask waitingTask2 = new WaitingTask();
    private int position;
    private int update;
    private int retry;
    public static final int UPATE_TASK = 1;
    public static final int DONT_UPDATE_TASK = 0;
    public static final int RETRY_TASK = 1;
    public static final int DONT_RETRY_TASK = 0;
    public static final int ALARM_TIME_CORRECT = 1;
    public static final int ALARM_TIME_WRONG = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_panel);

        Spinner typeSpinner = (Spinner) findViewById(R.id.type_spinner);
        typeSpinner.setOnItemSelectedListener(this);

        SimpleImageArrayAdapter adapter = new SimpleImageArrayAdapter(getApplicationContext(),
                new Integer[]{R.drawable.przypomnienie2, R.drawable.wydarzenie2, R.drawable.spotkanie3, R.drawable.urodziny2});
        typeSpinner.setAdapter(adapter);

        Spinner repeatSpinner = (Spinner) findViewById(R.id.repeat_spinner);
        repeatSpinner.setOnItemSelectedListener(this);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.spinner_item,
                new String[]{"Nie","Codziennie","Co tydzień","Co miesiąc","Co rok"});

        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        repeatSpinner.setAdapter(adapter2);

        Toolbar addToolbar = (Toolbar) findViewById(R.id.add_toolbar);

        setSupportActionBar(addToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent myIntent = getIntent();

        position = myIntent.getIntExtra("position",0);
        update = myIntent.getIntExtra("update",0);
        retry = myIntent.getIntExtra("retry",0);

        if(update == UPATE_TASK) setData();
        if(retry == RETRY_TASK) setData2();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_toolbar_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                Intent myIntent = new Intent(AddPanelActivity.this, ReminderPanelActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.confirm:
                if(update == DONT_UPDATE_TASK) {

                    int result = addData();
                    if(result == ALARM_TIME_CORRECT) {

                        Intent myIntent2 = new Intent(AddPanelActivity.this, ReminderPanelActivity.class);
                        startActivity(myIntent2);
                        finish();
                    }
                }
                else if(update == UPATE_TASK) {

                    int result = updateData();
                    if(result == ALARM_TIME_CORRECT) {

                        Intent myIntent3 = new Intent(AddPanelActivity.this, WaitingListActivity.class);
                        startActivity(myIntent3);
                        finish();
                    }
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {

        Spinner typeSpinner = (Spinner)parent;
        Spinner repeatSpinner = (Spinner)parent;

        if(typeSpinner.getId() == R.id.type_spinner) waitingTask.setType(pos);

        if(repeatSpinner.getId() == R.id.repeat_spinner) waitingTask.setRepeatType(pos);

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public int addData(){

        int result = ALARM_TIME_WRONG;

        EditText titleEditText = (EditText) findViewById(R.id.title_editText);
        waitingTask.setTitle(titleEditText.getText().toString());

        EditText detailsEditText = (EditText) findViewById(R.id.details_editText);
        waitingTask.setDetails(detailsEditText.getText().toString());

        Calendar calendar = Calendar.getInstance();

        DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);
        TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);

        int year = datePicker.getYear();
        int month = datePicker.getMonth();
        int day = datePicker.getDayOfMonth();

        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

        waitingTask.setAlarmTime(calendar.getTimeInMillis());

        month ++;

        String month2 = "";
        String minute2 = "";

        if (minute < 10) minute2 = "0" + minute;
        else minute2 = "" + minute;

        if (month < 10) month2 = "0" + month;
        else month2 = "" + month;

        waitingTask.setAlarmTimeText(day + "." + month2 + "." + year
                + " g. " + hour + ":" + minute2);

        result = setAlarm(calendar);

        if(result == ALARM_TIME_CORRECT) {

            DataBaseHelper dbHelper = new DataBaseHelper(this);
            dbHelper.addWaitingRecord(waitingTask);
            dbHelper.closeConnection(dbHelper);
        }

        return result;
    }

    public void setData(){

        DataBaseHelper dbHelper = new DataBaseHelper(this);
        ArrayList<WaitingTask> waitingList = new ArrayList<WaitingTask>();

        waitingList = dbHelper.getInfoFromWaitingList();

        if(position >= waitingList.size()) {

            Intent myIntent = new Intent(AddPanelActivity.this, WaitingListActivity.class);
            startActivity(myIntent);
            finish();

        } else waitingTask2 = waitingList.get(position);

        EditText titleEditText = (EditText) findViewById(R.id.title_editText);
        titleEditText.setText(waitingTask2.getTitle());

        EditText detailsEditText = (EditText) findViewById(R.id.details_editText);
        detailsEditText.setText(waitingTask2.getDetails());

        Spinner typeSpinner = (Spinner) findViewById(R.id.type_spinner);
        typeSpinner.setSelection(waitingTask2.getType());

        Spinner repeatSpinner = (Spinner) findViewById(R.id.repeat_spinner);
        repeatSpinner.setSelection(waitingTask2.getRepeatType());

        dbHelper.closeConnection(dbHelper);

    }

    public void setData2(){

        DataBaseHelper dbHelper = new DataBaseHelper(this);
        ArrayList<DoneTask> doneList = new ArrayList<>();

        doneList = dbHelper.getInfoFromDoneList();
        DoneTask done_task = doneList.get(position);

        EditText titleEditText = (EditText) findViewById(R.id.title_editText);
        titleEditText.setText(done_task.getTitle());

        EditText detailsEditText = (EditText) findViewById(R.id.details_editText);
        detailsEditText.setText(done_task.getDetails());

        Spinner typeSpinner = (Spinner) findViewById(R.id.type_spinner);
        typeSpinner.setSelection(done_task.getType());

        dbHelper.closeConnection(dbHelper);

    }

    public int updateData() {

        int result = ALARM_TIME_WRONG;

        EditText titleEditText = (EditText) findViewById(R.id.title_editText);
        waitingTask.setTitle(titleEditText.getText().toString());

        EditText detailsEditText = (EditText) findViewById(R.id.details_editText);
        waitingTask.setDetails(detailsEditText.getText().toString());

        Calendar calendar = Calendar.getInstance();

        DatePicker datePicker = (DatePicker) findViewById(R.id.datePicker);
        TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);

        int year = datePicker.getYear();
        int month = datePicker.getMonth();
        int day = datePicker.getDayOfMonth();

        int hour = timePicker.getCurrentHour();
        int minute = timePicker.getCurrentMinute();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

        waitingTask.setAlarmTime(calendar.getTimeInMillis());

        month ++;

        String month2 = "";
        String minute2 = "";

        if (minute < 10) minute2 = "0" + minute;
        else minute2 = "" + minute;

        if (month < 10) month2 = "0" + month;
        else month2 = "" + month;

        waitingTask.setAlarmTimeText(day + "." + month2 + "." + year
                + " g. " + hour + ":" + minute2);

        waitingTask.setAlarmNumber(waitingTask2.getAlarmNumber());

        DataBaseHelper dbHelper2 = new DataBaseHelper(this);
        ArrayList<WaitingTask> waitingList = new ArrayList<WaitingTask>();

        waitingList = dbHelper2.getInfoFromWaitingList();

        dbHelper2.closeConnection(dbHelper2);

        if(position < waitingList.size()) result = setAlarm(calendar);
            else result = ALARM_TIME_CORRECT;

        if(result == ALARM_TIME_CORRECT) {

            DataBaseHelper dbHelper = new DataBaseHelper(this);
            dbHelper.updateWaitingRecord(waitingTask, waitingTask2);
            dbHelper.closeConnection(dbHelper);
        }

        return result;
    }

    public int setAlarm(Calendar alarmTime) {

        Calendar timeNow = Calendar.getInstance();
        int result;

        if (timeNow.getTimeInMillis() >= alarmTime.getTimeInMillis()) {

            Toast.makeText(getApplicationContext(),
                    "Nie można ustawić daty, która już minęła",
                    Toast.LENGTH_SHORT).show();

            result = ALARM_TIME_WRONG;

        } else {

            AlarmManager alarmManager = (AlarmManager) getSystemService(getApplicationContext().ALARM_SERVICE);
            Intent myIntent = new Intent(this, AlarmReceiver.class);

            int alarmNumber = 0;

            if(update == DONT_UPDATE_TASK) {

                alarmNumber = getAlarmNumber();

            } else if(update == UPATE_TASK) {

                alarmNumber = waitingTask.getAlarmNumber();
            }

            PendingIntent pendingIntent = PendingIntent.getBroadcast(AddPanelActivity.this, alarmNumber, myIntent, FLAG_CANCEL_CURRENT);

            if(update == DONT_UPDATE_TASK) {

                waitingTask.setAlarmNumber(alarmNumber);

                rememberAlarmNumber(alarmNumber);
            }

            alarmManager.set(android.app.AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(), pendingIntent);

            result = ALARM_TIME_CORRECT;
        }

        return result;
    }

    public int getAlarmNumber() {

        int number = 0;
        SharedPreferences sp = getSharedPreferences("Alarm_number", Activity.MODE_PRIVATE);
        number = sp.getInt("number", 0);
        number++;

        return number;
    }

    public void rememberAlarmNumber(int number) {

        SharedPreferences sp = getSharedPreferences("Alarm_number", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("number", number);
        editor.commit();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(update == UPATE_TASK) setData();

    }

    @Override
    protected void onPause() {
        super.onPause();

        if(update == UPATE_TASK) setData();

    }

    @Override
    protected void onResume() {
        super.onPause();

        if(update == UPATE_TASK) setData();

    }

}
