package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class CounterAdapter extends ArrayAdapter<Counter> {
    private ArrayList<Counter> counterList;
    public static final int DISPLAY_CHECKBOX = 1;
    public static final int DONT_DISPLAY_CHECKBOX = 0;
    public static final int CHECKBOX_SELECTED = 1;
    public static final int CHECKBOX_NO_SELECTED = 0;

    public CounterAdapter(Context context, int textViewResourceId,
                         ArrayList<Counter> counterList) {
        super(context, textViewResourceId, counterList);
        this.counterList = new ArrayList<Counter>();
        this.counterList.addAll(counterList);
    }

    private class ViewHolder {
        FrameLayout counterIcon;
        TextView title;
        TextView state;
        CheckBox checkBox;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater)getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, null);

            holder = new ViewHolder();

            holder.counterIcon = (FrameLayout) convertView.findViewById(R.id.type_task);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.state = (TextView) convertView.findViewById(R.id.alarm_time_text);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            convertView.setTag(holder);

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    Counter cn = (Counter) cb.getTag();
                    cn.setSelected(cb.isChecked());

                    DataBaseHelper dbHelper = new DataBaseHelper(getContext());

                    if(cb.isChecked()) dbHelper.updateCounterCheck(cn,CHECKBOX_SELECTED);
                    else dbHelper.updateCounterCheck(cn,CHECKBOX_NO_SELECTED);

                    dbHelper.closeConnection(dbHelper);
                }
            });

        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Counter counter = counterList.get(position);

        holder.title.setText(counter.getTitle());
        holder.state.setText("" + counter.getState());

        holder.counterIcon.setBackgroundResource(R.drawable.counterr2);

        SharedPreferences sp = getContext().getSharedPreferences("checkBox_mode_counter_list", Activity.MODE_PRIVATE);
        int showCheck = sp.getInt("showCheck", 0);

        if(showCheck == DISPLAY_CHECKBOX) {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(counter.isSelected());
        }
        else if(showCheck == DONT_DISPLAY_CHECKBOX) holder.checkBox.setVisibility(View.INVISIBLE);

        holder.checkBox.setTag(counter);

        return convertView;

    }
}
