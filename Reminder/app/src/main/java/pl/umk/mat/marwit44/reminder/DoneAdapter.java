package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class DoneAdapter extends ArrayAdapter<DoneTask> {

    private ArrayList<DoneTask> doneList;
    public static final int DISPLAY_CHECKBOX = 1;
    public static final int DONT_DISPLAY_CHECKBOX = 0;
    public static final int CHECKBOX_SELECTED = 1;
    public static final int CHECKBOX_NO_SELECTED = 0;
    public static final int TASK_TYPE_REMIND = 0;
    public static final int TASK_TYPE_EVENT = 1;
    public static final int TASK_TYPE_MEET = 2;
    public static final int TASK_TYPE_BIRTHDAY = 3;

    public DoneAdapter(Context context, int textViewResourceId,
                       ArrayList<DoneTask> doneList) {
        super(context, textViewResourceId, doneList);
        this.doneList = new ArrayList<DoneTask>();
        this.doneList.addAll(doneList);
    }

    private class ViewHolder {
        FrameLayout typeTask;
        TextView title;
        TextView alarmTimeText;
        CheckBox checkBox;
        FrameLayout doneIcon;
        TextView doneTime;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if(convertView == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item3, null);

            holder = new ViewHolder();

            holder.typeTask = (FrameLayout) convertView.findViewById(R.id.type_task);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.alarmTimeText = (TextView) convertView.findViewById(R.id.alarm_time_text);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            holder.doneIcon = (FrameLayout) convertView.findViewById(R.id.done_icon);
            holder.doneTime = (TextView) convertView.findViewById(R.id.done_time);
            convertView.setTag(holder);

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    DoneTask dt = (DoneTask) cb.getTag();
                    dt.setSelected(cb.isChecked());

                    DataBaseHelper dbHelper = new DataBaseHelper(getContext());

                    if(cb.isChecked()) dbHelper.updateDoneCheck(dt,CHECKBOX_SELECTED);
                    else dbHelper.updateDoneCheck(dt,CHECKBOX_NO_SELECTED);

                    dbHelper.closeConnection(dbHelper);
                }
            });

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DoneTask doneTask = doneList.get(position);

        holder.title.setText(doneTask.getTitle());
        holder.alarmTimeText.setText(doneTask.getAlarmTimeText());
        int taskType = doneTask.getType();

        setTaskTypeInFrame(holder,taskType);

        SharedPreferences sp = getContext().getSharedPreferences("checkBox_mode_done_list", Activity.MODE_PRIVATE);
        int showCheck = sp.getInt("showCheck", 0);

        if (showCheck == DISPLAY_CHECKBOX) {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(doneTask.isSelected());
        } else if (showCheck == DONT_DISPLAY_CHECKBOX) holder.checkBox.setVisibility(View.INVISIBLE);

        holder.checkBox.setTag(doneTask);

        holder.doneIcon.setBackgroundResource(R.drawable.done);
        holder.doneTime.setText(doneTask.getDoneTime());

        return convertView;

    }

    public void setTaskTypeInFrame(ViewHolder holder, int taskType) {

        if(taskType == TASK_TYPE_REMIND) holder.typeTask.setBackgroundResource(R.drawable.przypomnienie2);
        else if(taskType == TASK_TYPE_EVENT) holder.typeTask.setBackgroundResource(R.drawable.wydarzenie2);
        else if(taskType == TASK_TYPE_MEET) holder.typeTask.setBackgroundResource(R.drawable.spotkanie3);
        else if(taskType == TASK_TYPE_BIRTHDAY) holder.typeTask.setBackgroundResource(R.drawable.urodziny2);

    }
}
