package pl.umk.mat.marwit44.reminder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class ReminderPanelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_panel);

        Toolbar reminderToolbar = (Toolbar) findViewById(R.id.reminder_toolbar);

        setSupportActionBar(reminderToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Button waitingButton = (Button) findViewById(R.id.waiting_button);
        waitingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ReminderPanelActivity.this, WaitingListActivity.class);
                startActivity(myIntent);
                finish();
            }
        });

        Button actualButton = (Button) findViewById(R.id.actual_button);
        actualButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ReminderPanelActivity.this, ActualListActivity.class);
                startActivity(myIntent);
                finish();
            }
        });

        Button doneButton = (Button) findViewById(R.id.done_button);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ReminderPanelActivity.this, DoneListActivity.class);
                startActivity(myIntent);
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reminder_toolbar_items, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                Intent myIntent = new Intent(ReminderPanelActivity.this, StartPanelActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.add_task:
                Intent myIntent2 = new Intent(ReminderPanelActivity.this, AddPanelActivity.class);
                startActivity(myIntent2);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

}
