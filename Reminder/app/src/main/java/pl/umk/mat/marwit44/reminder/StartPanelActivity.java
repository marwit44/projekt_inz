package pl.umk.mat.marwit44.reminder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

public class StartPanelActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_panel);

        FrameLayout reminderFrame = (FrameLayout) findViewById(R.id.reminder_frame);
        FrameLayout counterFrame = (FrameLayout) findViewById(R.id.counter_frame);

        reminderFrame.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
                Intent intent = new Intent(StartPanelActivity.this, ReminderPanelActivity.class);

                startActivity(intent);
                finish();
            }
        });

        counterFrame.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {

                Intent intent = new Intent(StartPanelActivity.this, CounterListActivity.class);

                startActivity(intent);
                finish();
            }
        });
    }
}
