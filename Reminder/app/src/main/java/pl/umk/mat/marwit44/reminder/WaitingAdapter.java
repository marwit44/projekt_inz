package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class WaitingAdapter extends ArrayAdapter<WaitingTask> {
    private ArrayList<WaitingTask> waitingList;
    public static final int DISPLAY_CHECKBOX = 1;
    public static final int DONT_DISPLAY_CHECKBOX = 0;
    public static final int CHECKBOX_SELECTED = 1;
    public static final int CHECKBOX_NO_SELECTED = 0;
    public static final int TASK_TYPE_REMIND = 0;
    public static final int TASK_TYPE_EVENT = 1;
    public static final int TASK_TYPE_MEET = 2;
    public static final int TASK_TYPE_BIRTHDAY = 3;
    public static final int NO_REPEAT_TYPE = 0;
    public static final int REPEAT_TYPE_DAY = 1;
    public static final int REPEAT_TYPE_WEEK = 2;
    public static final int REPEAT_TYPE_MONTH = 3;
    public static final int REPEAT_TYPE_YEAR = 4;

    public WaitingAdapter(Context context, int textViewResourceId,
                          ArrayList<WaitingTask> waitingList) {
        super(context, textViewResourceId, waitingList);
        this.waitingList = new ArrayList<WaitingTask>();
        this.waitingList.addAll(waitingList);
    }

    private class ViewHolder {
        FrameLayout typeTask;
        TextView title;
        TextView alarmTimeText;
        CheckBox checkBox;
        FrameLayout repeatType;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if(convertView == null) {
            LayoutInflater vi = (LayoutInflater)getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, null);

            holder = new ViewHolder();

            holder.typeTask = (FrameLayout) convertView.findViewById(R.id.type_task);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.alarmTimeText = (TextView) convertView.findViewById(R.id.alarm_time_text);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            holder.repeatType = (FrameLayout) convertView.findViewById(R.id.repeat_type);
            convertView.setTag(holder);

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    WaitingTask wt = (WaitingTask) cb.getTag();
                    wt.setSelected(cb.isChecked());

                    DataBaseHelper dbHelper = new DataBaseHelper(getContext());

                    if(cb.isChecked()) dbHelper.updateWaitingCheck(wt,CHECKBOX_SELECTED);
                    else dbHelper.updateWaitingCheck(wt,CHECKBOX_NO_SELECTED);

                    dbHelper.closeConnection(dbHelper);
                }
            });

        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        WaitingTask waitingTask = waitingList.get(position);

        holder.title.setText(waitingTask.getTitle());
        holder.alarmTimeText.setText(waitingTask.getAlarmTimeText());
        int taskType = waitingTask.getType();

        setTaskTypeInFrame(holder,taskType);

        SharedPreferences sp = getContext().getSharedPreferences("checkBox_mode_waiting_list", Activity.MODE_PRIVATE);
        int showCheck = sp.getInt("showCheck", 0);

        if(showCheck == DISPLAY_CHECKBOX) {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(waitingTask.isSelected());

            int repeat = waitingTask.getRepeatType();

            if(repeat != 0) holder.repeatType.setVisibility(View.INVISIBLE);
        }
        else if(showCheck == DONT_DISPLAY_CHECKBOX) {
            holder.checkBox.setVisibility(View.INVISIBLE);
            holder.repeatType.setVisibility(View.VISIBLE);

            int repeat = waitingTask.getRepeatType();
            setRepeatTypeInFrame(holder,repeat);

        }

        holder.checkBox.setTag(waitingTask);

        return convertView;

    }

    public void setTaskTypeInFrame(ViewHolder holder, int taskType) {

        if(taskType== TASK_TYPE_REMIND) holder.typeTask.setBackgroundResource(R.drawable.przypomnienie2);
        else if(taskType == TASK_TYPE_EVENT) holder.typeTask.setBackgroundResource(R.drawable.wydarzenie2);
        else if(taskType == TASK_TYPE_MEET) holder.typeTask.setBackgroundResource(R.drawable.spotkanie3);
        else if(taskType == TASK_TYPE_BIRTHDAY) holder.typeTask.setBackgroundResource(R.drawable.urodziny2);

    }

    public void setRepeatTypeInFrame(ViewHolder holder, int repeat) {

        if(repeat == NO_REPEAT_TYPE) holder.repeatType.setVisibility(View.INVISIBLE);
        else if(repeat == REPEAT_TYPE_DAY) holder.repeatType.setBackgroundResource(R.drawable.powt_d5);
        else if(repeat == REPEAT_TYPE_WEEK) holder.repeatType.setBackgroundResource(R.drawable.powt_t3);
        else if(repeat == REPEAT_TYPE_MONTH) holder.repeatType.setBackgroundResource(R.drawable.powt_m3);
        else if(repeat == REPEAT_TYPE_YEAR) holder.repeatType.setBackgroundResource(R.drawable.powt_r3);

    }
}
