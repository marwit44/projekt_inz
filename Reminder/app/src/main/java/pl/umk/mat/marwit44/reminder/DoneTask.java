package pl.umk.mat.marwit44.reminder;

public class DoneTask {

    private int _ID;
    private String title;
    private int type;
    private String details;
    private long alarmTime;
    private String alarmTimeText;
    private String doneTime;
    private boolean selected = false;

    public int get_ID() {
        return _ID;
    }

    public void set_ID(int _ID) {
        this._ID = _ID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public long getAlarmTime() {
        return alarmTime;
    }

    public void setAlarmTime(long alarmTime) {
        this.alarmTime = alarmTime;
    }

    public String getAlarmTimeText() {
        return alarmTimeText;
    }

    public void setAlarmTimeText(String alarmTimeText) {
        this.alarmTimeText = alarmTimeText;
    }

    public String getDoneTime() {
        return doneTime;
    }

    public void setDoneTime(String doneTime) {
        this.doneTime = doneTime;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
