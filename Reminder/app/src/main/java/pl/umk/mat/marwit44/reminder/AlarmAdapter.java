package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class AlarmAdapter extends ArrayAdapter<WaitingTask> {

    private ArrayList<WaitingTask> alarmList;
    public static final int TASK_TYPE_REMIND = 0;
    public static final int TASK_TYPE_EVENT = 1;
    public static final int TASK_TYPE_MEET = 2;
    public static final int TASK_TYPE_BIRTHDAY = 3;
    public static final int NO_REPEAT_TYPE = 0;
    public static final int REPEAT_TYPE_DAY = 1;
    public static final int REPEAT_TYPE_WEEK = 2;
    public static final int REPEAT_TYPE_MONTH = 3;
    public static final int REPEAT_TYPE_YEAR = 4;

    public AlarmAdapter(Context context, int textViewResourceId,
                          ArrayList<WaitingTask> alarmList) {
        super(context, textViewResourceId, alarmList);
        this.alarmList = new ArrayList<WaitingTask>();
        this.alarmList.addAll(alarmList);
    }

    private class ViewHolder {
        FrameLayout typeTask;
        TextView title;
        TextView alarmTimeText;
        FrameLayout repeatType;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if(convertView == null) {
            LayoutInflater vi = (LayoutInflater)getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item2, null);

            holder = new ViewHolder();

            holder.typeTask = (FrameLayout) convertView.findViewById(R.id.type_task);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.alarmTimeText = (TextView) convertView.findViewById(R.id.alarm_time_text);
            holder.repeatType = (FrameLayout) convertView.findViewById(R.id.repeat_type);

            convertView.setTag(holder);

        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        WaitingTask alarmTask = alarmList.get(position);

        holder.title.setText(alarmTask.getTitle());
        holder.alarmTimeText.setText(""+alarmTask.getAlarmTimeText());

        int taskType = alarmTask.getType();
        setTaskTypeInFrame(holder,taskType);

        int repeat = alarmTask.getRepeatType();
        setRepeatTypeInFrame(holder,repeat);

        return convertView;

    }

    public void setTaskTypeInFrame(ViewHolder holder, int taskType) {

        if(taskType == TASK_TYPE_REMIND) holder.typeTask.setBackgroundResource(R.drawable.przypomnienie2);
        else if(taskType == TASK_TYPE_EVENT) holder.typeTask.setBackgroundResource(R.drawable.wydarzenie2);
        else if(taskType == TASK_TYPE_MEET) holder.typeTask.setBackgroundResource(R.drawable.spotkanie3);
        else if(taskType == TASK_TYPE_BIRTHDAY) holder.typeTask.setBackgroundResource(R.drawable.urodziny2);

    }

    public void setRepeatTypeInFrame(ViewHolder holder, int repeat) {

        if(repeat != NO_REPEAT_TYPE) holder.repeatType.setVisibility(View.VISIBLE);
        else holder.repeatType.setVisibility(View.INVISIBLE);

        if(repeat == REPEAT_TYPE_DAY) holder.repeatType.setBackgroundResource(R.drawable.powt_d5);
        else if(repeat == REPEAT_TYPE_WEEK) holder.repeatType.setBackgroundResource(R.drawable.powt_t3);
        else if(repeat == REPEAT_TYPE_MONTH) holder.repeatType.setBackgroundResource(R.drawable.powt_m3);
        else if(repeat == REPEAT_TYPE_YEAR) holder.repeatType.setBackgroundResource(R.drawable.powt_r3);

    }
}
