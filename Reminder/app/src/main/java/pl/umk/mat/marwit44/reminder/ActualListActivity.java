package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ActualListActivity extends AppCompatActivity {

    private ActualAdapter dataAdapter;
    private ArrayList<ActualTask> actualList = new ArrayList<>();
    private int showCheck;
    private Menu menu;
    private AlertDialog alertDialog;
    private AlertDialog alertDialog2;
    private int alertDialogType;
    public static final int DELETE_ALERT_DIALOG_SHOW = 1;
    public static final int ACCEPT_ALERT_DIALOG_SHOW = 2;
    public static final int DONT_SHOW_ALERT_DIALOG = 0;
    public static final int DISPLAY_CHECKBOX = 1;
    public static final int DONT_DISPLAY_CHECKBOX = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actual_list);

        displayListView();

        if (savedInstanceState != null) {
            alertDialogType = savedInstanceState.getInt("alertDialogType");
        }

        Toolbar actualToolbar = (Toolbar) findViewById(R.id.actual_toolbar);

        setSupportActionBar(actualToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        SharedPreferences sp = getSharedPreferences("checkBox_mode_actual_list", Activity.MODE_PRIVATE);
        showCheck = sp.getInt("showCheck", 0);

        if(alertDialogType == DELETE_ALERT_DIALOG_SHOW) deleteDialog();
        else if(alertDialogType == ACCEPT_ALERT_DIALOG_SHOW) acceptDialog();

    }

    private void displayListView() {

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        actualList = dbHelper.getInfoFromActualList();

        dataAdapter = new ActualAdapter(this, R.layout.list_item, actualList);

        ListView actualListView = (ListView) findViewById(R.id.actual_list_view);
        actualListView.setAdapter(dataAdapter);

        actualListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {

                if(showCheck == DONT_DISPLAY_CHECKBOX) {

                    Intent myIntent = new Intent(ActualListActivity.this, ActualDetailsPanelActivity.class);
                    myIntent.putExtra("position", position);
                    startActivity(myIntent);
                    finish();
                }
            }
        });

        dbHelper.closeConnection(dbHelper);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                showCheck = DISPLAY_CHECKBOX;
                showCheckBox();

                Intent myIntent = new Intent(ActualListActivity.this, ReminderPanelActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.check:
                showCheckBox();
                return true;

            case R.id.accept:
                acceptDialog();
                return true;

            case R.id.delete:
                deleteDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.menu = menu;
        getMenuInflater().inflate(R.menu.actual_toolbar_items, menu);

        if(showCheck == DISPLAY_CHECKBOX) setOptionTitle(R.id.check, "Wstecz");

        return true;
    }

    private void hideOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

    private void setOptionTitle(int id, String title)
    {
        MenuItem item = menu.findItem(id);
        item.setTitle(title);
    }

    private void setOptionIcon(int id, int iconRes)
    {
        MenuItem item = menu.findItem(id);
        item.setIcon(iconRes);
    }

    public void showCheckBox() {

        if(showCheck == DONT_DISPLAY_CHECKBOX) {

            showCheck = DISPLAY_CHECKBOX;

            SharedPreferences sp = getSharedPreferences("checkBox_mode_actual_list", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putInt("showCheck", showCheck);
            editor.commit();

            displayListView();

            setOptionTitle(R.id.check, "Wstecz");


        } else if(showCheck == DISPLAY_CHECKBOX) {

            showCheck = DONT_DISPLAY_CHECKBOX;

            SharedPreferences sp = getSharedPreferences("checkBox_mode_actual_list", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putInt("showCheck", showCheck);
            editor.commit();

            DataBaseHelper dbHelper = new DataBaseHelper(this);
            dbHelper.deleteActualChecks();
            dbHelper.closeConnection(dbHelper);

            displayListView();

            setOptionTitle(R.id.check, "Zaznacz");

        }
    }

    public void deleteDialog() {

        if(showCheck == DONT_DISPLAY_CHECKBOX) {

            Toast.makeText(getApplicationContext(),
                    "Wybierz zadnia do usunięcia", Toast.LENGTH_LONG).show();

        } else if(showCheck == DISPLAY_CHECKBOX) {

            alertDialogType = DELETE_ALERT_DIALOG_SHOW;

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.delete_ask_panel, null);
            builder.setView(dialogView);

            alertDialog = builder.create();
            alertDialog.show();
        }

    }

    public void acceptDialog() {

        if(showCheck == DONT_DISPLAY_CHECKBOX) {

            Toast.makeText(getApplicationContext(),
                    "Wybierz zadnia do akceptacji", Toast.LENGTH_LONG).show();

        } else if(showCheck == DISPLAY_CHECKBOX) {

            alertDialogType = ACCEPT_ALERT_DIALOG_SHOW;

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.accept_ask_panel, null);
            builder.setView(dialogView);

            alertDialog2 = builder.create();
            alertDialog2.show();
        }

    }

    public void deleteData() {


        final ArrayList<ActualTask> actualList = this.actualList;
        final DataBaseHelper dbHelper = new DataBaseHelper(this);

        for (int i = 0; i < actualList.size(); i++) {

            final ActualTask actualTask = actualList.get(i);

            if (actualTask.isSelected()) {
                dbHelper.deleteActualRecord(actualTask);
            }

        }

        dbHelper.deleteActualChecks();

        showCheck = DISPLAY_CHECKBOX;
        showCheckBox();

        displayListView();

        dbHelper.closeConnection(dbHelper);
    }

    public void acceptData() {


        final ArrayList<ActualTask> actualList = this.actualList;
        final DataBaseHelper dbHelper = new DataBaseHelper(this);

        for (int i = 0; i < actualList.size(); i++) {

            final ActualTask actualTask = actualList.get(i);

            if (actualTask.isSelected()) {

                dbHelper.deleteActualRecord(actualTask);
                dbHelper.addDoneRecord(actualTask);
            }

        }

        dbHelper.deleteActualChecks();

        showCheck = DISPLAY_CHECKBOX;
        showCheckBox();

        displayListView();

        dbHelper.closeConnection(dbHelper);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(alertDialogType == DELETE_ALERT_DIALOG_SHOW) alertDialog.cancel();
            else if(alertDialogType == ACCEPT_ALERT_DIALOG_SHOW) alertDialog2.cancel();

        savedInstanceState.putInt("alertDialogType", alertDialogType);
    }

    @Override
    public void onBackPressed() {
        showCheck = DISPLAY_CHECKBOX;
        showCheckBox();

        System.exit(0);
    }

    public void delete_yes(View view) {
        deleteData();
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();
    }

    public void delete_no(View view) {
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();
    }

    public void accept_yes(View view) {
        acceptData();
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog2.cancel();
    }

    public void accept_no(View view) {
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog2.cancel();
    }

    @Override
    protected void onStop() {
        super.onStop();

        displayListView();

    }

    @Override
    protected void onPause() {
        super.onPause();

        displayListView();

    }

    @Override
    protected void onResume() {
        super.onPause();

        displayListView();

    }
}
