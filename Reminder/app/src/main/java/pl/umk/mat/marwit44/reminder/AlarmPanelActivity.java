package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class AlarmPanelActivity extends AppCompatActivity {

    private int startApp;
    private int mode = -1;
    private MediaPlayer mySound;
    private int stopMusic = 0;
    private AlarmAdapter dataAdapter;
    ArrayList<WaitingTask> alarmList = new ArrayList<>();
    private Calendar timeNow;
    private int isPlaying;
    private int newAlarm;
    private int check;
    public static final int OPEN_NEW_APP_WINDOW = 1;
    public static final int DONT_OPEN_NEW_APP_WINDOW = 0;
    public static final int SET_CHECKBOX = 1;
    public static final int MUSIC_PLAYED = 1;
    public static final int MUSIC_NOT_PLAYED = 0;
    public static final int MUSIC_IS_PLAYING = 0;
    public static final int MUSIC_STOPPED = 1;
    public static final int UPDATE_ALARM_PANEL = 2;
    public static final int DONT_UPDATE_ALARM_PANEL = 0;
    public static final int NO_REPEAT = 0;
    public static final int DAY_REPEAT = 1;
    public static final int WEEK_REPEAT = 2;
    public static final int MONTH_REPEAT = 3;
    public static final int YEAR_REPEAT = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_panel);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        SharedPreferences sp = getSharedPreferences("alarm_panel_settings", Activity.MODE_PRIVATE);
        isPlaying = sp.getInt("isPlaying",MUSIC_NOT_PLAYED);
        check = sp.getInt("check", DONT_OPEN_NEW_APP_WINDOW);

        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox_startApp);

        if(isPlaying == MUSIC_NOT_PLAYED) play();
        else Toast.makeText(getApplicationContext(),
                "Nowe alarmy zotały aktywowane",
                Toast.LENGTH_SHORT).show();

        if(check == SET_CHECKBOX) {
            checkBox.setChecked(true);
            startApp = OPEN_NEW_APP_WINDOW;
        }

        SharedPreferences.Editor editor = sp.edit();
        editor.putInt("isPlaying", 0);
        editor.putInt("check",0);
        editor.commit();

        timeNow = Calendar.getInstance();
        long timeNowInMillis = timeNow.getTimeInMillis();

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        ArrayList<WaitingTask> waitingList = dbHelper.getInfoFromWaitingList();

        for (int i = 0; i < waitingList.size(); i++) {
            WaitingTask waitingTask = waitingList.get(i);

            Calendar alarmTime = Calendar.getInstance();
            alarmTime.setTimeInMillis(waitingTask.getAlarmTime());

            if (alarmTime.getTimeInMillis() <= timeNowInMillis) {
                alarmList.add(waitingTask);
            }
        }

        displayListView();

        dbHelper.closeConnection(dbHelper);

        checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                if (cb.isChecked() == true) startApp = OPEN_NEW_APP_WINDOW;
                else startApp = DONT_OPEN_NEW_APP_WINDOW;
            }
        });

        Button stopAlarm = (Button) findViewById(R.id.stop_alarm);
        stopAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stopMusic == MUSIC_IS_PLAYING) stopMusic();
            }
        });

        Button closeWindow = (Button) findViewById(R.id.close_window);
        closeWindow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (stopMusic == MUSIC_IS_PLAYING) stopMusic();
                checkNew();
                updateState();

                if(newAlarm == UPDATE_ALARM_PANEL) {

                    Intent intent = getIntent();

                    finish();
                    startActivity(intent);

                }  else if(startApp == OPEN_NEW_APP_WINDOW) {

                            Intent myIntent = new Intent(AlarmPanelActivity.this, ReminderPanelActivity.class);
                            startActivity(myIntent);
                            finish();

                            } else finish();

            }
        });
    }

    public void displayListView() {


        dataAdapter = new AlarmAdapter(this, R.layout.list_item2, alarmList);

        ListView alarmListView = (ListView) findViewById(R.id.alarm_list_view);
        alarmListView.setAdapter(dataAdapter);

        alarmListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
            }
        });
    }

    public void updateState() {

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        for(int i = 0; i < alarmList.size(); i++) {

            WaitingTask task = alarmList.get(i);

            dbHelper.addActualRecord(task);

            int repeat = task.getRepeatType();

            if(repeat == NO_REPEAT) dbHelper.deleteWaitingRecord(task);
            else if(repeat == DAY_REPEAT) {
                setRepeating(task, DAY_REPEAT);
            }
            else if(repeat == WEEK_REPEAT) {
                setRepeating(task, WEEK_REPEAT);
            }
            else if(repeat == MONTH_REPEAT) {
                setRepeating(task, MONTH_REPEAT);
            }
            else if(repeat == YEAR_REPEAT) {
                setRepeating(task, YEAR_REPEAT);
            }

        }

        dbHelper.closeConnection(dbHelper);
    }

    public void setRepeating(WaitingTask task, int mode) {

        WaitingTask wt = task;
        long alarmTime = wt.getAlarmTime();

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(alarmTime);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        if(mode == DAY_REPEAT) {

            long interval = 86400000;
            alarmTime += interval;

            calendar.setTimeInMillis(alarmTime);

        } else if(mode == WEEK_REPEAT) {

            long  interval = 604800000;
            alarmTime += interval;

            calendar.setTimeInMillis(alarmTime);

        } else if(mode == MONTH_REPEAT) {
            if(month == 11) {
                month = 0;
                year ++;
                calendar.set(Calendar.YEAR, year);
            }
            else month ++;

            calendar.set(Calendar.MONTH, month);

        } else if(mode == YEAR_REPEAT) {
            year ++;

            calendar.set(Calendar.YEAR, year);
        }

        AlarmManager alarmManager = (AlarmManager) getSystemService(getApplicationContext().ALARM_SERVICE);
        Intent myIntent = new Intent(this, AlarmReceiver.class);

        int alarmNumber = wt.getAlarmNumber();

        PendingIntent pendingIntent = PendingIntent.getBroadcast(AlarmPanelActivity.this, alarmNumber, myIntent, FLAG_CANCEL_CURRENT);

        alarmManager.set(android.app.AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);

        wt.setAlarmTime(calendar.getTimeInMillis());

        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minute = calendar.get(Calendar.MINUTE);

        month ++;

        String month2 = "";
        String minute2 = "";

        if (minute < 10) minute2 = "0" + minute;
        else minute2 = "" + minute;

        if (month < 10) month2 = "0" + month;
        else month2 = "" + month;

        wt.setAlarmTimeText(day + "." + month2 + "." + year
                + " g. " + hour + ":" + minute2);

        DataBaseHelper dbHelper = new DataBaseHelper(this);
        dbHelper.updateAlarmTime(wt);
        dbHelper.closeConnection(dbHelper);
    }

    public void checkNew() {

        int oldAlarmsNumber = alarmList.size();
        Calendar timeNow = Calendar.getInstance();
        long timeNowInMillis = timeNow.getTimeInMillis();

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        ArrayList<WaitingTask> waitingList = dbHelper.getInfoFromWaitingList();
        ArrayList<WaitingTask> alarmList2 = new ArrayList<>();

        for (int i = 0; i < waitingList.size(); i++) {
            WaitingTask waitingTask = waitingList.get(i);

            Calendar alarmTime = Calendar.getInstance();
            alarmTime.setTimeInMillis(waitingTask.getAlarmTime());

            if (alarmTime.getTimeInMillis() <= timeNowInMillis) {
                alarmList2.add(waitingTask);
            }
        }

        int oldAndNewAlarmsNumber = alarmList2.size();

        if(oldAlarmsNumber < oldAndNewAlarmsNumber) {
            newAlarm = UPDATE_ALARM_PANEL;
            SharedPreferences sp = getSharedPreferences("alarm_panel_settings", Activity.MODE_PRIVATE);

            SharedPreferences.Editor editor = sp.edit();
            editor.putInt("isPlaying", MUSIC_PLAYED);
            editor.putInt("check",startApp);
            editor.commit();
        }

        dbHelper.closeConnection(dbHelper);
    }

    public void play()
    {
        Intent myIntent = new Intent(this.getApplicationContext(), AlarmPanelActivity.class);

        AudioManager am = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

        switch (am.getRingerMode()) {
            case AudioManager.RINGER_MODE_SILENT:
                mode = 0;
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                mode = 1;
                break;
            case AudioManager.RINGER_MODE_NORMAL:
                mode = 2;
                break;
        }

        if(mode == 2) {

            mySound = MediaPlayer.create(this, R.raw.tropicalal_ringtone);
            mySound.start();
            mySound.setLooping(true);

        } else if(mode == 1) {

            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            long[] pattern = {0, 1000, 1000};
            vibrator.vibrate(pattern,0);
        }
    }

    public void stopMusic()
    {
        if((mySound != null) && (mode == 2)) {
            mySound.stop();
            mySound.release();
        } else if(mode == 1) {
            Vibrator vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.cancel();
        }

        stopMusic = MUSIC_STOPPED;
    }

    @Override
    public void onBackPressed() {

        if(mySound != null) mySound.release();
        checkNew();
        updateState();

        if(newAlarm == UPDATE_ALARM_PANEL) {

            Intent intent = getIntent();

            finish();
            startActivity(intent);
        }  else if(startApp == OPEN_NEW_APP_WINDOW) {

            Intent myIntent = new Intent(AlarmPanelActivity.this, ReminderPanelActivity.class);
            startActivity(myIntent);
        }

        System.exit(0);
    }
}
