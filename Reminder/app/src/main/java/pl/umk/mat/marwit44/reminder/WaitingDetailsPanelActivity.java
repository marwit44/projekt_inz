package pl.umk.mat.marwit44.reminder;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class WaitingDetailsPanelActivity extends AppCompatActivity {

    private int position;
    public static final int WRONG_POS_NUMBER = 1;
    public static final int CORRECT_POS_NUMBER = 0;
    public static final int UPDATE_TASK = 1;
    public static final int TASK_TYPE_REMIND = 0;
    public static final int TASK_TYPE_EVENT = 1;
    public static final int TASK_TYPE_MEET = 2;
    public static final int TASK_TYPE_BIRTHDAY = 3;
    public static final int NO_REPEAT_TYPE = 0;
    public static final int REPEAT_TYPE_DAY = 1;
    public static final int REPEAT_TYPE_WEEK = 2;
    public static final int REPEAT_TYPE_MONTH = 3;
    public static final int REPEAT_TYPE_YEAR = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_details_panel);

        Toolbar waitingDetailsToolbar = (Toolbar) findViewById(R.id.waiting_details_toolbar);

        setSupportActionBar(waitingDetailsToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent myIntent = getIntent();

        position = myIntent.getIntExtra("position",0);

        setData(position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.show_det_toolbar_items, menu);
        return true;
    }

    public void setData(int position){

        DataBaseHelper dbHelper = new DataBaseHelper(this);
        ArrayList<WaitingTask> waitingList = new ArrayList<>();
        WaitingTask waitingTask = new WaitingTask();

        waitingList = dbHelper.getInfoFromWaitingList();

        if(position >= waitingList.size()) {

            Intent myIntent = new Intent(WaitingDetailsPanelActivity.this, WaitingListActivity.class);
            startActivity(myIntent);
            finish();

        } else waitingTask = waitingList.get(position);

        TextView titleText = (TextView) findViewById(R.id.title_text);
        TextView detailsText = (TextView) findViewById(R.id.details_text);
        TextView dataText = (TextView) findViewById(R.id.data_text);

        titleText.setText(waitingTask.getTitle());
        detailsText.setText(waitingTask.getDetails());
        dataText.setText(waitingTask.getAlarmTimeText());

        FrameLayout taskTypeFrame = (FrameLayout) findViewById(R.id.task_type_frame);

        FrameLayout repeatTypeFrame = (FrameLayout) findViewById(R.id.repeat_type_frame);

        int type = waitingTask.getType();
        setTaskTypeInFrame(taskTypeFrame,type);

        int repeat = waitingTask.getRepeatType();
        setRepeatTypeInFrame(repeatTypeFrame,repeat);

        dbHelper.closeConnection(dbHelper);

    }

    public void setTaskTypeInFrame(FrameLayout taskTypeFrame, int type) {

        if(type == TASK_TYPE_REMIND) taskTypeFrame.setBackgroundResource(R.drawable.przypomnienie2);
        else if(type == TASK_TYPE_EVENT) taskTypeFrame.setBackgroundResource(R.drawable.wydarzenie2);
        else if(type == TASK_TYPE_MEET) taskTypeFrame.setBackgroundResource(R.drawable.spotkanie3);
        else if(type == TASK_TYPE_BIRTHDAY) taskTypeFrame.setBackgroundResource(R.drawable.urodziny2);

    }

    public void setRepeatTypeInFrame(FrameLayout repeatTypeFrame, int repeat) {

        if(repeat == NO_REPEAT_TYPE) repeatTypeFrame.setBackgroundResource(android.R.color.transparent);
        else if(repeat == REPEAT_TYPE_DAY) repeatTypeFrame.setBackgroundResource(R.drawable.powt_d5);
        else if(repeat == REPEAT_TYPE_WEEK) repeatTypeFrame.setBackgroundResource(R.drawable.powt_t3);
        else if(repeat == REPEAT_TYPE_MONTH) repeatTypeFrame.setBackgroundResource(R.drawable.powt_m3);
        else if(repeat == REPEAT_TYPE_YEAR) repeatTypeFrame.setBackgroundResource(R.drawable.powt_r3);

    }

    public int setData2(int position) {

        DataBaseHelper dbHelper = new DataBaseHelper(this);
        ArrayList<WaitingTask> waitingList = new ArrayList<>();

        int result = CORRECT_POS_NUMBER;

        waitingList = dbHelper.getInfoFromWaitingList();

        if(position >= waitingList.size()) {

            result = WRONG_POS_NUMBER;
            Intent myIntent = new Intent(WaitingDetailsPanelActivity.this, WaitingListActivity.class);
            startActivity(myIntent);
            finish();
        }

        return result;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                Intent myIntent = new Intent(WaitingDetailsPanelActivity.this, WaitingListActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.update:
                int result = setData2(position);

                if(result == CORRECT_POS_NUMBER) {

                    Intent myIntent2 = new Intent(WaitingDetailsPanelActivity.this, AddPanelActivity.class);
                    myIntent2.putExtra("position", position);
                    myIntent2.putExtra("update", UPDATE_TASK);
                    startActivity(myIntent2);
                    finish();
                }

                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        setData(position);

    }

    @Override
    protected void onPause() {
        super.onPause();

        setData(position);

    }

    @Override
    protected void onResume() {
        super.onPause();

        setData(position);

    }
}
