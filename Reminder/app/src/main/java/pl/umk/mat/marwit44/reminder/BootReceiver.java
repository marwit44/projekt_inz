package pl.umk.mat.marwit44.reminder;


import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;
import java.util.Calendar;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class BootReceiver extends BroadcastReceiver {

    public static final int START_ALARM_PANEL = 1;
    public static final int DONT_START_ALARM_PANEL = 0;

    @Override
    public void onReceive(Context context, Intent intent) {

        Calendar timeNow = Calendar.getInstance();
        long timeNowInMillis = timeNow.getTimeInMillis();

        DataBaseHelper dbHelper = new DataBaseHelper(context);
        ArrayList<WaitingTask> waitingList = new ArrayList<>();
        int startAlarmPanel = DONT_START_ALARM_PANEL;

        waitingList = dbHelper.getInfoFromWaitingList();

        for(int i = 0; i < waitingList.size(); i++) {

            WaitingTask waitingTask = waitingList.get(i);

            Calendar alarmTime = Calendar.getInstance();
            alarmTime.setTimeInMillis(waitingTask.getAlarmTime());

            int alarmNumber = waitingTask.getAlarmNumber();

            if(alarmTime.getTimeInMillis() > timeNowInMillis) {

                setAlarmAfterReboot(context,alarmNumber,alarmTime);

            } else {

                startAlarmPanel = START_ALARM_PANEL;
            }
        }

        if(startAlarmPanel == START_ALARM_PANEL) {

            Intent myIntent = new Intent(context,AlarmPanelActivity.class);
            myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(myIntent);
        }
    }

    public void setAlarmAfterReboot(Context context, int alarmNumber, Calendar alarmTime ) {

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
        Intent myIntent = new Intent(context, AlarmReceiver.class);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, alarmNumber, myIntent, FLAG_CANCEL_CURRENT);

        alarmManager.set(android.app.AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(), pendingIntent);
    }
}