package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class ActualAdapter extends ArrayAdapter<ActualTask> {

    private ArrayList<ActualTask> actualList;
    public static final int DISPLAY_CHECKBOX = 1;
    public static final int DONT_DISPLAY_CHECKBOX = 0;
    public static final int CHECKBOX_SELECTED = 1;
    public static final int CHECKBOX_NO_SELECTED = 0;
    public static final int TASK_TYPE_REMIND = 0;
    public static final int TASK_TYPE_EVENT = 1;
    public static final int TASK_TYPE_MEET = 2;
    public static final int TASK_TYPE_BIRTHDAY = 3;

    public ActualAdapter(Context context, int textViewResourceId,
                          ArrayList<ActualTask> actualList) {
        super(context, textViewResourceId, actualList);
        this.actualList = new ArrayList<ActualTask>();
        this.actualList.addAll(actualList);
    }

    private class ViewHolder {
        FrameLayout typeTask;
        TextView title;
        TextView alarmTimeText;
        CheckBox checkBox;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        if(convertView == null) {
            LayoutInflater vi = (LayoutInflater)getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, null);

            holder = new ViewHolder();

            holder.typeTask = (FrameLayout) convertView.findViewById(R.id.type_task);
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.alarmTimeText = (TextView) convertView.findViewById(R.id.alarm_time_text);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
            convertView.setTag(holder);

            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    ActualTask at = (ActualTask) cb.getTag();
                    at.setSelected(cb.isChecked());

                    DataBaseHelper dbHelper = new DataBaseHelper(getContext());

                    if(cb.isChecked()) dbHelper.updateActualCheck(at,CHECKBOX_SELECTED);
                        else dbHelper.updateActualCheck(at,CHECKBOX_NO_SELECTED);

                    dbHelper.closeConnection(dbHelper);
                }
            });

        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        ActualTask actualTask = actualList.get(position);

        holder.title.setText(actualTask.getTitle());
        holder.alarmTimeText.setText(actualTask.getAlarmTimeText());
        int Tasktype = actualTask.getType();

        setTaskTypeInFrame(holder,Tasktype);

        SharedPreferences sp = getContext().getSharedPreferences("checkBox_mode_actual_list", Activity.MODE_PRIVATE);
        int showCheck = sp.getInt("showCheck", 0);

        if(showCheck == DISPLAY_CHECKBOX) {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(actualTask.isSelected());
        }
        else if(showCheck == DONT_DISPLAY_CHECKBOX) holder.checkBox.setVisibility(View.INVISIBLE);

        holder.checkBox.setTag(actualTask);

        return convertView;

    }

    public void setTaskTypeInFrame(ViewHolder holder, int Tasktype) {

        if(Tasktype == TASK_TYPE_REMIND) holder.typeTask.setBackgroundResource(R.drawable.przypomnienie2);
        else if(Tasktype == TASK_TYPE_EVENT) holder.typeTask.setBackgroundResource(R.drawable.wydarzenie2);
        else if(Tasktype == TASK_TYPE_MEET) holder.typeTask.setBackgroundResource(R.drawable.spotkanie3);
        else if(Tasktype == TASK_TYPE_BIRTHDAY) holder.typeTask.setBackgroundResource(R.drawable.urodziny2);

    }
}
