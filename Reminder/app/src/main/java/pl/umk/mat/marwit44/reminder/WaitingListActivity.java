package pl.umk.mat.marwit44.reminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.app.PendingIntent.FLAG_CANCEL_CURRENT;

public class WaitingListActivity extends AppCompatActivity {

    private WaitingAdapter dataAdapter;
    private ArrayList<WaitingTask> waitingList = new ArrayList<WaitingTask>();
    private int showCheck;
    private Menu menu;
    private AlertDialog alertDialog;
    private int alertDialogType;
    public static final int DELETE_ALERT_DIALOG_SHOW = 1;
    public static final int DONT_SHOW_ALERT_DIALOG = 0;
    public static final int DISPLAY_CHECKBOX = 1;
    public static final int DONT_DISPLAY_CHECKBOX = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiting_list);

        displayListView();

        if (savedInstanceState != null) {
            alertDialogType = savedInstanceState.getInt("alertDialogType");
        }

        Toolbar waitingToolbar = (Toolbar) findViewById(R.id.waiting_toolbar);

        setSupportActionBar(waitingToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        SharedPreferences sp = getSharedPreferences("checkBox_mode_waiting_list", Activity.MODE_PRIVATE);
        showCheck = sp.getInt("showCheck", 0);

        if(alertDialogType == DELETE_ALERT_DIALOG_SHOW) deleteDialog();

    }

    public void displayListView() {

        DataBaseHelper dbHelper = new DataBaseHelper(this);

        waitingList = dbHelper.getInfoFromWaitingList();

        dataAdapter = new WaitingAdapter(this, R.layout.list_item, waitingList);

        ListView waitingListView = (ListView) findViewById(R.id.waiting_list_view);
        waitingListView.setAdapter(dataAdapter);

        waitingListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {

                if(showCheck == DONT_DISPLAY_CHECKBOX) {

                    Intent myIntent = new Intent(WaitingListActivity.this, WaitingDetailsPanelActivity.class);
                    myIntent.putExtra("position", position);
                    startActivity(myIntent);
                    finish();

                }
            }
        });

        dbHelper.closeConnection(dbHelper);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.menu = menu;
        getMenuInflater().inflate(R.menu.waiting_toolbar_items, menu);

        if(showCheck == DISPLAY_CHECKBOX) setOptionTitle(R.id.check, "Wstecz");

        return true;
    }

    private void hideOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(false);
    }

    private void showOption(int id)
    {
        MenuItem item = menu.findItem(id);
        item.setVisible(true);
    }

    private void setOptionTitle(int id, String title)
    {
        MenuItem item = menu.findItem(id);
        item.setTitle(title);
    }

    private void setOptionIcon(int id, int iconRes)
    {
        MenuItem item = menu.findItem(id);
        item.setIcon(iconRes);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                showCheck = DISPLAY_CHECKBOX;
                showCheckBox();
                Intent myIntent = new Intent(WaitingListActivity.this, ReminderPanelActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.check:
                showCheckBox();
                return true;

            case R.id.delete:
                deleteDialog();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void showCheckBox() {

        if(showCheck == DONT_DISPLAY_CHECKBOX) {

            showCheck = DISPLAY_CHECKBOX;

            SharedPreferences sp = getSharedPreferences("checkBox_mode_waiting_list", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putInt("showCheck", showCheck);
            editor.commit();

            displayListView();

            setOptionTitle(R.id.check, "Wstecz");


        } else if(showCheck == DISPLAY_CHECKBOX) {

            showCheck = DONT_DISPLAY_CHECKBOX;

            SharedPreferences sp = getSharedPreferences("checkBox_mode_waiting_list", Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp.edit();

            editor.putInt("showCheck", showCheck);
            editor.commit();

            DataBaseHelper dbHelper = new DataBaseHelper(this);
            dbHelper.deleteWaitingChecks();
            dbHelper.closeConnection(dbHelper);

            displayListView();

            setOptionTitle(R.id.check, "Zaznacz");

        }
    }

    public void deleteDialog() {

        if(showCheck == DONT_DISPLAY_CHECKBOX) {

            Toast.makeText(getApplicationContext(),
                    "Wybierz zadnia do usunięcia", Toast.LENGTH_LONG).show();

        } else if(showCheck == DISPLAY_CHECKBOX) {

            alertDialogType = DELETE_ALERT_DIALOG_SHOW;

            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.delete_ask_panel, null);
            builder.setView(dialogView);

            alertDialog = builder.create();
            alertDialog.show();
        }

    }

    public void deleteData() {


        final ArrayList<WaitingTask> waitingList = this.waitingList;
        final DataBaseHelper dbHelper = new DataBaseHelper(this);

        for (int i = 0; i < waitingList.size(); i++) {

            final WaitingTask waitingTask = waitingList.get(i);

            if (waitingTask.isSelected()) {

                AlarmManager alarmManager = (AlarmManager) getSystemService(getApplicationContext().ALARM_SERVICE);
                Intent cancelIntent = new Intent(WaitingListActivity.this, AlarmReceiver.class);

                PendingIntent pendingIntent = PendingIntent.getBroadcast(WaitingListActivity.this, waitingTask.getAlarmNumber(),cancelIntent,FLAG_CANCEL_CURRENT);
                alarmManager.cancel(pendingIntent);

                dbHelper.deleteWaitingRecord(waitingTask);
            }
        }

        dbHelper.deleteWaitingChecks();

        showCheck = DISPLAY_CHECKBOX;
        showCheckBox();

        displayListView();

        dbHelper.closeConnection(dbHelper);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {

        if(alertDialogType == DELETE_ALERT_DIALOG_SHOW) alertDialog.cancel();

        savedInstanceState.putInt("alertDialogType", alertDialogType);
    }

    @Override
    public void onBackPressed() {
        showCheck = DISPLAY_CHECKBOX;
        showCheckBox();
        System.exit(0);
    }

    @Override
    protected void onStop() {
        super.onStop();

        displayListView();

    }

    @Override
    protected void onPause() {
        super.onPause();

        displayListView();

    }

    @Override
    protected void onResume() {
        super.onPause();

        displayListView();

    }

    public void delete_yes(View view) {
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        deleteData();
        alertDialog.cancel();
    }

    public void delete_no(View view) {
        alertDialogType = DONT_SHOW_ALERT_DIALOG;
        alertDialog.cancel();
    }
}
