package pl.umk.mat.marwit44.reminder;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class DoneDetailsPanelActivity extends AppCompatActivity {

    private int position;
    private Menu menu;
    private DoneTask doneTask;
    public static final int RETRY_TASK = 1;
    public static final int TASK_TYPE_REMIND = 0;
    public static final int TASK_TYPE_EVENT = 1;
    public static final int TASK_TYPE_MEET = 2;
    public static final int TASK_TYPE_BIRTHDAY = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_done_details_panel);

        Toolbar doneDetailsToolbar = (Toolbar) findViewById(R.id.done_details_toolbar);

        setSupportActionBar(doneDetailsToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent myIntent = getIntent();

        position = myIntent.getIntExtra("position", 0);

        setData(position);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.show_det_do_toolbar_items, menu);
        return true;
    }

    public void setData(int position) {

        DataBaseHelper dbHelper = new DataBaseHelper(this);
        ArrayList<DoneTask> doneList = new ArrayList<>();

        doneList = dbHelper.getInfoFromDoneList();

        doneTask = doneList.get(position);

        TextView titleTextDone = (TextView) findViewById(R.id.title_text_do);
        TextView detailsTextDone = (TextView) findViewById(R.id.details_text_do);
        TextView alarmDataTextDone = (TextView) findViewById(R.id.alarm_data_text_do);
        TextView doneData = (TextView) findViewById(R.id.done_data_do);

        titleTextDone.setText(doneTask.getTitle());
        detailsTextDone.setText(doneTask.getDetails());
        alarmDataTextDone.setText(doneTask.getAlarmTimeText());
        doneData.setText(doneTask.getDoneTime());

        FrameLayout taskTypeFrameDone = (FrameLayout) findViewById(R.id.task_type_frame_do);

        int type = doneTask.getType();
        setTaskTypeInFrame(taskTypeFrameDone,type);

        dbHelper.closeConnection(dbHelper);

    }

    public void setTaskTypeInFrame(FrameLayout taskTypeFrame, int type) {

        if(type == TASK_TYPE_REMIND) taskTypeFrame.setBackgroundResource(R.drawable.przypomnienie2);
        else if(type == TASK_TYPE_EVENT) taskTypeFrame.setBackgroundResource(R.drawable.wydarzenie2);
        else if(type == TASK_TYPE_MEET) taskTypeFrame.setBackgroundResource(R.drawable.spotkanie3);
        else if(type == TASK_TYPE_BIRTHDAY) taskTypeFrame.setBackgroundResource(R.drawable.urodziny2);

    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                Intent myIntent = new Intent(DoneDetailsPanelActivity.this, DoneListActivity.class);
                startActivity(myIntent);
                finish();
                return true;

            case R.id.retry:
                Intent myIntent2 = new Intent(DoneDetailsPanelActivity.this, AddPanelActivity.class);
                myIntent2.putExtra("retry",RETRY_TASK);
                myIntent2.putExtra("position",position);
                startActivity(myIntent2);
                finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
