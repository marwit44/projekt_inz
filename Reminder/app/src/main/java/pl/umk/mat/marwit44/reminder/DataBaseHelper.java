package pl.umk.mat.marwit44.reminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;

public class DataBaseHelper extends SQLiteOpenHelper {

    final static String dataBaseName = "Reminder_app2.db";

    final static String tableName = "Waiting_tasks";
    final static String tableName2 = "Actual_tasks";
    final static String tableName3 = "Done_tasks";
    final static String tableName4 = "Counter_list";

    public DataBaseHelper(Context context) {
        super(context, dataBaseName, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + tableName + "(_id integer primary key autoincrement, title text," +
                "type integer, details text, alarm_time integer, alarm_time_text text, pintent integer, check_2 integer, repeat integer);");

        db.execSQL("CREATE TABLE " + tableName2 + "(_id integer primary key autoincrement, title text," +
                "type integer, details text, alarm_time integer, alarm_time_text text, check_2 integer);");

        db.execSQL("CREATE TABLE " + tableName3 + "(_id integer primary key autoincrement, title text," +
                "type integer, details text, alarm_time integer, alarm_time_text text, done_time integer, done_time_text text, check_2 integer);");

        db.execSQL("CREATE TABLE " + tableName4 + "(_id integer primary key autoincrement, title text," +
                "state integer, step integer, check_2 integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void addWaitingRecord(WaitingTask task) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues conVal = new ContentValues();

        conVal.put("title", task.getTitle());
        conVal.put("type", task.getType());
        conVal.put("details", task.getDetails());
        conVal.put("alarm_time", task.getAlarmTime());
        conVal.put("alarm_time_text", task.getAlarmTimeText());
        conVal.put("pintent", task.getAlarmNumber());
        conVal.put("check_2",0);
        conVal.put("repeat", task.getRepeatType());

        db.insertOrThrow(tableName, null, conVal);
    }

    public void deleteWaitingRecord(WaitingTask task) {

        SQLiteDatabase db = getWritableDatabase();
        String[] args = {"" + task.get_ID()};

        db.rawQuery("DELETE FROM " + tableName + " WHERE _id = ?", args).moveToFirst();
    }

    public void updateWaitingRecord(WaitingTask newTask, WaitingTask oldTask) {

        SQLiteDatabase db = getWritableDatabase();

        String[] args = {newTask.getTitle(), "" + newTask.getType(), newTask.getDetails(), "" + newTask.getAlarmTime(),
                newTask.getAlarmTimeText(), "" + newTask.getRepeatType(), "" + oldTask.get_ID(), oldTask.getAlarmTimeText()};

        db.rawQuery("UPDATE " + tableName + " SET title = ?, type = ?, details = ?, alarm_time = ?, alarm_time_text = ?, repeat = ? " +
                "WHERE _id = ? and alarm_time_text = ?;", args).moveToFirst();

    }

    public void updateAlarmTime(WaitingTask task) {

        SQLiteDatabase db = getWritableDatabase();

        String[] args = {"" + task.getAlarmTime(), task.getAlarmTimeText(), "" + task.get_ID()};

        db.rawQuery("UPDATE " + tableName + " SET alarm_time = ?, alarm_time_text = ?" +
                "WHERE _id = ?;", args).moveToFirst();

    }

    public ArrayList<WaitingTask> getInfoFromWaitingList() {
        ArrayList<WaitingTask> waitingList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + tableName + " ORDER BY alarm_time asc;", null);

        while (cursor.moveToNext()) {
            WaitingTask waitingTask = new WaitingTask();

            waitingTask.set_ID(cursor.getInt(0));
            waitingTask.setTitle(cursor.getString(1));
            waitingTask.setType(cursor.getInt(2));
            waitingTask.setDetails(cursor.getString(3));
            waitingTask.setAlarmTime(cursor.getLong(4));
            waitingTask.setAlarmTimeText(cursor.getString(5));
            waitingTask.setAlarmNumber(cursor.getInt(6));

            int check = cursor.getInt(7);

            if(check == 1) waitingTask.setSelected(true);
            else waitingTask.setSelected(false);

            waitingTask.setRepeatType(cursor.getInt(8));

            waitingList.add(waitingTask);
        }

        cursor.close();

        return waitingList;
    }

    public void updateWaitingCheck(WaitingTask task, int check) {

        SQLiteDatabase db = getWritableDatabase();

        String[] args = {"" + check, "" + task.get_ID(), task.getAlarmTimeText()};

        db.rawQuery("UPDATE " + tableName + " SET check_2 = ? WHERE _id = ? and alarm_time_text = ?;", args).moveToFirst();

    }

    public void deleteWaitingChecks() {

        SQLiteDatabase db = getWritableDatabase();

        int check = 0;
        String[] args = {"" + check};

        db.rawQuery("UPDATE " + tableName + " SET check_2 = ?;", args).moveToFirst();

    }


    public void addActualRecord(WaitingTask task) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues conVal = new ContentValues();

        conVal.put("title", task.getTitle());
        conVal.put("type", task.getType());
        conVal.put("details", task.getDetails());
        conVal.put("alarm_time", task.getAlarmTime());
        conVal.put("alarm_time_text", task.getAlarmTimeText());
        conVal.put("check_2",0);

        db.insertOrThrow(tableName2, null, conVal);
    }

    public ArrayList<ActualTask> getInfoFromActualList() {
        ArrayList<ActualTask> actualList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + tableName2 + " ORDER BY alarm_time desc;", null);

        while (cursor.moveToNext()) {
            ActualTask actualTask = new ActualTask();

            actualTask.set_ID(cursor.getInt(0));
            actualTask.setTitle(cursor.getString(1));
            actualTask.setType(cursor.getInt(2));
            actualTask.setDetails(cursor.getString(3));
            actualTask.setAlarmTime(cursor.getLong(4));
            actualTask.setAlarmTimeText(cursor.getString(5));

            int check = cursor.getInt(6);

            if(check == 1) actualTask.setSelected(true);
            else actualTask.setSelected(false);

            actualList.add(actualTask);
        }

        cursor.close();

        return actualList;
    }

    public void updateActualCheck(ActualTask task, int check) {

        SQLiteDatabase db = getWritableDatabase();

        String[] args = {"" + check, "" + task.get_ID(), task.getAlarmTimeText()};

        db.rawQuery("UPDATE " + tableName2 + " SET check_2 = ? WHERE _id = ? and alarm_time_text = ?;", args).moveToFirst();

    }

    public void deleteActualChecks() {

        SQLiteDatabase db = getWritableDatabase();

        int check = 0;
        String[] args = {"" + check};

        db.rawQuery("UPDATE " + tableName2 + " SET check_2 = ?;", args).moveToFirst();

    }

    public void deleteActualRecord(ActualTask task) {

        SQLiteDatabase db = getWritableDatabase();
        String[] args = {"" + task.get_ID()};

        db.rawQuery("DELETE FROM " + tableName2 + " WHERE _id = ?", args).moveToFirst();
    }

    public void addDoneRecord(ActualTask task) {

        Calendar calendar = Calendar.getInstance();
        long doneTime = calendar.getTimeInMillis();

        String doneTimeText = setDoneTime(calendar);

        SQLiteDatabase db = getWritableDatabase();

        ContentValues conVal = new ContentValues();

        conVal.put("title", task.getTitle());
        conVal.put("type", task.getType());
        conVal.put("details", task.getDetails());
        conVal.put("alarm_time", task.getAlarmTime());
        conVal.put("alarm_time_text", task.getAlarmTimeText());
        conVal.put("done_time", doneTime);
        conVal.put("done_time_text", doneTimeText);
        conVal.put("check_2", 0);

        db.insertOrThrow(tableName3, null, conVal);
    }

    public ArrayList<DoneTask> getInfoFromDoneList() {
        ArrayList<DoneTask> doneList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + tableName3 + " ORDER BY done_time desc;", null);

        while (cursor.moveToNext()) {
            DoneTask doneTask = new DoneTask();

            doneTask.set_ID(cursor.getInt(0));
            doneTask.setTitle(cursor.getString(1));
            doneTask.setType(cursor.getInt(2));
            doneTask.setDetails(cursor.getString(3));
            doneTask.setAlarmTime(cursor.getLong(4));
            doneTask.setAlarmTimeText(cursor.getString(5));
            doneTask.setDoneTime(cursor.getString(7));

            int check = cursor.getInt(8);

            if(check == 1) doneTask.setSelected(true);
            else doneTask.setSelected(false);

            doneList.add(doneTask);
        }

        cursor.close();

        return doneList;
    }

    public void updateDoneCheck(DoneTask task, int check) {

        SQLiteDatabase db = getWritableDatabase();

        String[] args = {"" + check, "" + task.get_ID(), task.getAlarmTimeText()};

        db.rawQuery("UPDATE " + tableName3 + " SET check_2 = ? WHERE _id = ? and alarm_time_text = ?;", args).moveToFirst();

    }

    public void deleteDoneChecks() {

        SQLiteDatabase db = getWritableDatabase();

        int check = 0;
        String[] args = {"" + check};

        db.rawQuery("UPDATE " + tableName3 + " SET check_2 = ?;", args).moveToFirst();

    }

    public void deleteDoneRecord(DoneTask task) {

        SQLiteDatabase db = getWritableDatabase();
        String[] args = {"" + task.get_ID()};

        db.rawQuery("DELETE FROM " + tableName3 + " WHERE _id = ?", args).moveToFirst();
    }

    public String setDoneTime(Calendar calendar) {

        String doneTime = "";

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        month ++;

        String month2 = "";
        String minute2 = "";

        if (minute < 10) minute2 = "0" + minute;
        else minute2 = "" + minute;

        if (month < 10) month2 = "0" + month;
        else month2 = "" + month;

        doneTime = day + "." + month2 + "." + year + " g. " + hour + ":" + minute2;

        return doneTime;
    }

    public void addCounterRecord(String title) {

        SQLiteDatabase db = getWritableDatabase();

        ContentValues conVal = new ContentValues();

        conVal.put("title",title);
        conVal.put("state",0);
        conVal.put("step",1);
        conVal.put("check_2",0);

        db.insertOrThrow(tableName4, null, conVal);
    }

    public ArrayList<Counter> getInfoFromCounterList() {
        ArrayList<Counter> counterList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM " + tableName4 + " ORDER BY title asc;", null);

        while (cursor.moveToNext()) {
            Counter counter = new Counter();

            counter.set_ID(cursor.getInt(0));
            counter.setTitle(cursor.getString(1));
            counter.setState(cursor.getInt(2));
            counter.setStep(cursor.getInt(3));

            int check = cursor.getInt(4);

            if(check == 1) counter.setSelected(true);
            else counter.setSelected(false);

            counterList.add(counter);
        }

        cursor.close();

        return counterList;
    }

    public void updateCounterCheck(Counter counter, int check) {

        SQLiteDatabase db = getWritableDatabase();

        String[] args = {"" + check, "" + counter.get_ID(), "" + counter.getState()};

        db.rawQuery("UPDATE " + tableName4 + " SET check_2 = ? WHERE _id = ? and state = ?;", args).moveToFirst();

    }

    public void deleteCounterChecks() {

        SQLiteDatabase db = getWritableDatabase();

        int check = 0;
        String[] args = {"" + check};

        db.rawQuery("UPDATE " + tableName4 + " SET check_2 = ?;", args).moveToFirst();

    }

    public void updateCounterRecord(Counter counter) {

        SQLiteDatabase db = getWritableDatabase();

        String[] args = {"" + counter.getState(), "" + counter.getStep(),"" + counter.get_ID(),};

        db.rawQuery("UPDATE " + tableName4 + " SET state = ?, step = ? WHERE _id = ?", args).moveToFirst();

    }

    public void deleteCounterRecord(Counter counter) {

        SQLiteDatabase db = getWritableDatabase();
        String[] args = {"" + counter.get_ID()};

        db.rawQuery("DELETE FROM " + tableName4 + " WHERE _id = ?", args).moveToFirst();
    }

    public void updateCounterTitle(Counter counter, String title) {

        SQLiteDatabase db = getWritableDatabase();

        String[] args = {title, "" + counter.get_ID(),};

        db.rawQuery("UPDATE " + tableName4 + " SET title = ? WHERE _id = ?", args).moveToFirst();

    }

    public void closeConnection(DataBaseHelper db) {

        try {
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (db != null) {
                try {
                    db.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

